%global dropdir %(pkg-config libpcsclite --variable usbdropdir 2>/dev/null)

Name:             ccid
Version:          1.4.29
Release:          4
Summary:          Provide a generic USB CCID driver and ICCD
License:          LGPLv2+
URL:              https://ccid.apdu.fr/files/

Source0:          https://ccid.apdu.fr/files/ccid-%{version}.tar.bz2

BuildRequires:    perl-interpreter perl-Getopt-Long libusb1-devel gnupg2 gcc git
BuildRequires:    pcsc-lite-devel >= 1.8.9
Requires(post):   systemd
Requires(postun): systemd
Requires:         pcsc-lite%{?_isa} >= 1.8.9
Provides:         pcsc-ifd-handler
Obsoletes:        pcsc-lite-ccid
Provides:         pcsc-lite-ccid

%description
This package provides the source code for a generic USB CCID (Chip/Smart Card
Interface Devices) driver and ICCD (Integrated Circuit(s) Card Devices).See the
USB CCID and ICCD specifications from the USB working group.

%prep
%autosetup -n ccid-%{version} -p1 -S git

%build
%configure --enable-twinserial
%make_build

%install
make install DESTDIR=$RPM_BUILD_ROOT
cp -p src/openct/LICENSE LICENSE.openct

%post
/bin/systemctl try-restart pcscd.service &>/dev/null || :

%postun
/bin/systemctl try-restart pcscd.service &>/dev/null || :

%files
%doc AUTHORS ChangeLog README
%license COPYING LICENSE.openct
%{dropdir}/ifd-ccid.bundle/
%{dropdir}/serial/
%config(noreplace) %{_sysconfdir}/reader.conf.d/libccidtwin

%changelog
* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.4.29-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Repackage

* Sat Aug 31 2019 huangzheng <huangzheng22@huawei.com> - 1.4.29-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:openEuler Debranding
